#define BAUD 9600

#define DEVICE_UUID "placeholder-uuid"
#define BOARD_VERSION "1.0.0"


enum {
  COMMAND_INFO = 0,
  COMMAND_RESET,
  COMMAND_ERROR_CODE,
  COMMAND_SENTINEL
};

enum {
  ERR_NONE = 0,
  ERR_COMMAND_NOT_FOUND,
  ERR_SENTINEL
};

uint8_t error_code;


void info() {
  // number of lines, simplifies parsing on receiving side
  Serial.write(5);
  // ID
  Serial.write(DEVICE_ID);
  // board version
  Serial.write(BOARD_VERSION);
  // add commands here
  Serial.write("COMMAND_INFO COMMAND_RESET COMMAND_ERROR_CODE");
  // date & time of compilation
  Serial.println(__DATE__);
  Serial.println(__TIME__);
}


void reset() {
  error_code = 0;
}


void setup() {
  error_code = 0;
  Serial.begin(BAUD);
}


void loop() {
  while (Serial.available() == 0) {}

  uint8_t command = Serial.read();

  // split command into command & payload
  uint8_t payload = command >> 4;
  command = command & 0xF;
  
  switch (command) {
    case COMMAND_INFO:
    {
      info();
    }
    break;

    case COMMAND_RESET:
    {
      reset();
    }
    break;

    case COMMAND_ERROR_CODE:
    {
      Serial.write(error_code);
      error_code = 0;
    }
    break;

    default:
    {
      error_code = ERR_COMMAND_NOT_FOUND;
    }
    break;
  }
}
